# Arch Linux UEFI installation guide

Official guide: https://wiki.archlinux.org/index.php/Installation_guide



## Preparing bootable USB drive with Arch Linux image

1. Download ISO: https://www.archlinux.org/download/
1. Create a bootable USB drive from: https://rufus.akeo.ie/
1. After USB is ready, boot you machine from it


## Connect to the internet

If using wired network, it is probably already configured.

### Switch to NetworkManager:

```bash
pacaman -S networkmanager
systemctl disable dhcpcd
systemctl enable NetworkManager
```

### Wireless

1. Check the name of wireless interface:

    ```bash
    ip addr
    ```

1. If you dont know the SSID of you access point run:

```bash
iw dev <INTERFACE> scan | grep SSID
```

1. Connect to an access point:

```bash
wpa_supplicant -B -i <INTERFACE> -c <(wpa_passphrase <SSID> <PASSWORD>)
```

1. Get network ip address

```bash
dhcpcd <INTERFACE>
```

## Partition setup

1. Check name of the device that is going to be used for installation:
```bash
fdisk -l
```

1. Clear device partitions
```bash
gdisk /dev/<DISK-DEVICE>
x
y
y
```

1. Create partitions
```bash
cgdisk /dev/<DISK-DEVICE>
```
1. Create a `boot` partition with code: `ef00`. Recommended size: 1024MiB
1. Create a `root` partition with code `8300`. Use remaining space with swap is not desired. 
1. Format `boot` partition:
```bash
mkfs.fat -F32 /dev/<DISK-DEVICE><BOOT-PARTITION>
```
1. Format `root` partition:
```bash
mkfs.ext4 /dev/<DISK-DEVICE><ROOT-PARTITION>
```
1. Mount `root` partition:
```bash
mount /dev/<DISK-DEVICE><ROOT-PARTITION> /mnt
```
1. Create folder for `boot` partition and mount it:
```bash
mkdir /mnt/boot
mount /dev/<DISK-DEVICE><ROOT-PARTITION> /mnt/boot
```

## Pacman setup

1. Backup pacman's mirrorlist and update it with the top fastest mirrors for your location
```bash
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
rankmirrors -n 5 /etc/pacman.d/mirrorlist.bak > /etc/pacman.d/mirrorlist
```
1. Install pacman an other essential packages:
```bash
pacstrap -i /mnt base base-devel
```

## Extra pacakges

Enable wireless configuration after you start booting from your installation:
```bash
pacman -S wpa_supplicant
```

## Gnome setup

1. Install gnome:

``


## Region setup

1. Enable NTP
```bash
# timedatectl set-ntp true
```

1. 
Temporary reference: https://www.youtube.com/watch?v=iF7Y8IH5A3M&index=1&list=PLqBnXqkMhGIbql4Y9W6-1vUNk8MonoXv-
